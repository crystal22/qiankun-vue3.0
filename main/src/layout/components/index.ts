/*
 * @Author: lizhijie429
 * @Date: 2021-06-19 13:50:56
 * @LastEditors: lizhijie429
 * @LastEditTime: 2021-06-19 17:19:02
 * @Description:
 */
export { default as HeaderNav } from "./Header.vue";
export { default as SideNav } from "./SideNav.vue";
export { default as Tabs } from "./Tabs.vue";
