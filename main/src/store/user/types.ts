/*
 * @Author: lizhijie429
 * @Date: 2021-07-22 20:25:53
 * @LastEditors: lizhijie429
 * @LastEditTime: 2021-07-23 08:59:23
 * @Description:
 */
export enum UserMutationsType {
  UPDATE_USER_INFO = "UPDATE_USER_INFO",
  UPDATE_USER_INFO_ITEM = "UPDATE_USER_INFO_ITEM",
  UPDATE_GLOBAL_CONFIG = "UPDATE_GLOBAL_CONFIG",
}
